FROM slicer/slicer-base

ARG SLICER_VERSION

RUN \
  cd /usr/src/  && \
  git clone https://github.com/Slicer/Slicer.git && cd Slicer && git checkout $SLICER_VERSION && cd .. && \
  cd /usr/src/Slicer-build && \
  echo "PROCESSOR_COUNT: $(grep -c processor /proc/cpuinfo)" && \

  ../Slicer-build/BuildSlicer.sh

ENV FLYWHEEL='/flywheel/v0'
RUN mkdir -p ${FLYWHEEL}
WORKDIR ${FLYWHEEL}

RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-py39_4.10.3-Linux-x86_64.sh \
    && bash Miniconda3-py39_4.10.3-Linux-x86_64.sh -b \
    && rm -f Miniconda3-py39_4.10.3-Linux-x86_64.sh

RUN pip install --upgrade pip
RUN pip install poetry

