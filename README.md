# flywheel/fw-gear-slicer-base

```
export SLICER_VERSION=<X.Y.Z>
docker build --build-arg SLICER_VERSION=$SLICER_VERSION -t flywheel/fw-gear-slicer-base:$SLICER_VERSION
docker push
```
